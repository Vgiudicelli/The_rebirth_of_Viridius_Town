Exécuter le fichier edit_tiles.py (SANS LE MODIFIER).

Dans le Shell d'exécution de python, exécuter la fonction create_tile() pour créer une nouvelle tuile, et sélectionner les paramètres (utiliser environ 200 pour la résolution) ou edit_existing_tile("...") pour modifier une tuile.


L'éditeur se compose de deux parties :
- partie édition à gauche
- partie visualisation à droite

Caler l'image sur les points à l'aide du zoom (molette de la souris) et de cliqués glissé
Note : pour déplacer uniquement l'image, et pas les points, appuyer sur Maj en même temps.

Si besoin, passer en mode édition avancée en appuyant sur espace, pour déplacer les points individuellement (et donc déformer l'image). La touche Tab permet de passer au point suivant, backspace permet de supprimer un point (dans l'éditeur uniquement), et echap de revenir à la configuration initiale.

Pour recadrer l'image finale, effectuer un cliqué glissé dans la partie visualisation.

Enfin, utilisez la touche entrer pour enregistrer.


Note : dans le cas de tuiles destinées aux bâtiments, celle-ci doivent avoir le même nom que dans le fichier batiments.csv