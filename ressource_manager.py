import pygame as pg
#from buildings import load_buildings

class RessourceManager:
    _IS_ALREADY_INSTANCIED_ = False
    
    def __init__(self):
        # we can create only one instance
        if RessourceManager._IS_ALREADY_INSTANCIED_:
            raise Exception("RessourceManager already exist")
        RessourceManager._IS_ALREADY_INSTANCIED_ = True

        # ressources
        self.ressources ={
            "Wood": 10,
            "Stone": 10,
            "Energy": 100,
            "Gold": 100,
            "Iron": 0,
            "Searching":0,
            "Inhabitants": 1,
            "CE":1000,
            }

        #for key in self.ressources:
            #self.ressources[key] = 1e100

        #self.buildings_builders = load_buildings(self)
