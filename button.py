import pygame as pg
from utils import FORCE_ALL_RECT_VISIBLE

rect_transparancy = 100 if FORCE_ALL_RECT_VISIBLE else 0

class Button:
    def __init__(self, dim, pos, text, font, command, **options):
        self.surfaces = [pg.Surface(dim, pg.SRCALPHA),
                         pg.Surface(dim, pg.SRCALPHA)]
        if "center" in options:
            self.rect = self.surfaces[0].get_rect(center = pos)
        else:
            self.rect = self.surfaces[0].get_rect(topleft = pos)

        color = options["color"] if "color" in options else (255, 255, 255)

        texts = [font.render(text, True, color),
                 font.render(text, True, color)]
        rect_text = texts[0].get_rect(center = (self.rect.width/2, self.rect.height/2))
        texts[0].set_alpha(190)

        for surf, text in zip(self.surfaces, texts):
            surf.fill((0,0,0,rect_transparancy))
            surf.blit(text, rect_text)

        
        self._command_ = command


    def blit_on(self, surface):
        surface.blit(self.surfaces[self.is_mouse_in()], self.rect)
#        if self.is_mouse_in():
 #           self.surface.blit(self._text_0_, self._rect_text_0_)
#        else:
 #           self.surface.blit(self._text_1_, self._rect_text_1_)

    def is_mouse_in(self):
        x, y = pg.mouse.get_pos()
        return (self.rect.left <= x <= self.rect.right
                and self.rect.top <= y <= self.rect.bottom)
        
    def react_events(self, events):
        for event in events:
            if event.type == pg.MOUSEBUTTONDOWN and self.is_mouse_in():
                self._command_()


if __name__ == "__main__":
    import main
