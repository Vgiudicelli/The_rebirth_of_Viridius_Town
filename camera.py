"""
gestion du champ de vision
"""
import utils
import pygame as pg

centre_screen = [c/2 for c in utils.size_screen]

class Camera:
    def __init__(self, surface, corrige_pos):
        position = [v/2 for v in utils.size_screen]
        self.rect = surface.get_rect(center = position)
        self._surface_ = surface
        self._corrige_pos_ = corrige_pos
        self.update()


    def update(self):
        mouse_position = pg.mouse.get_pos()

        rel_pos = [(p-c)/c for (p,c) in zip(mouse_position, centre_screen)]
        
        motion = [-10 * r ** 15 for r in rel_pos]     # TODO adapter
        
        self.rect = self._corrige_pos_(self.rect.move(*motion))
        
if __name__ == "__main__":
    import main
    self = main.app.game.world.camera
