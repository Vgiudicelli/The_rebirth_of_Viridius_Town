import pygame as pg
import utils
from button import Button
from music import Music
from game import Game

CTE = utils.CTE

class MainMenu:
    FPS = 30
    
    def __init__(self):
        self.size = [2/3*v for v in utils.size_screen]
        w, h = self.size

        self.clock = pg.time.Clock()

        pg.display.set_caption("The rebirth of Viridio", "Viridio")

        self.bg_image = utils.load_image("background.jpg")
        self.bg_image = utils.resize(self.bg_image, self.size)

        x_center = w / 2

        self.build_title(w, h)
        self.build_btn(w, h)

        self._running_ = False

        self.frame_option = Options(self.size)
        self.game = Game()

    def set_screen(self):
        pg.display.quit()
        self.screen = pg.display.set_mode(self.size, flags = 0)
        self.frame_option.set_screen(self.screen)

    def build_title(self, w, h):
        self.surface_title = pg.Surface((w, .25*h), pg.SRCALPHA)
        if utils.FORCE_ALL_RECT_VISIBLE:
            self.surface_title.fill((0,0,0,100))
        self.rect_title = self.surface_title.get_rect(topleft = (0,0))

        text_1 = pg.font.Font(utils.defaut_font, int(4*CTE)).render("The Rebirth of", True, "black")
        text_2 = pg.font.Font(utils.defaut_font, int(8*CTE)).render("V I R I D I O", True, "black")
        rect_1 = text_1.get_rect(center=(w/2, 0.05 * h))
        rect_2 = text_2.get_rect(center=(w/2, 0.17 * h))

        self.surface_title.blit(text_1, rect_1)
        self.surface_title.blit(text_2, rect_2)

        line_color = (0,0,0)
        line_pos = [0.39*w,0.09*h,0.22*w,2]
        pg.draw.rect(self.surface_title,
                     line_color,
                     line_pos)

    def build_btn(self, w, h):
        size_btn = (w / 3, h / 8)
        font_buttons = pg.font.Font(utils.defaut_font, int(5*CTE))
        
        self.btns = [
            Button(size_btn, (w / 2, .4 * h), "PLAY",    font_buttons, self.play, center = True),
            Button(size_btn, (w / 2, .6 * h), "OPTIONS", font_buttons, self.options, center = True),
            Button(size_btn, (w / 2, .8 * h), "QUIT",    font_buttons, self.quit, center = True)
                     ]
        


    def quit(self):
        self._running_ = False

    def play(self):
        self._actions_.append(lambda:
            (self.game.run(),
             Music.menu(),
             self.set_screen()))
        
    def options(self):
        self._actions_.append(lambda:
            (self.frame_option.run(),
             Music.menu()))
        
    def run(self):
        if self._running_:
            raise Exception("MainMenu is already running")

        self.set_screen()
        
        Music.menu()

        self._running_ = True
        while self._running_:
            self.screen.blit(self.bg_image, (0,0))
            self.screen.blit(self.surface_title, self.rect_title)
            for btn in self.btns:
                btn.blit_on(self.screen)

            events = pg.event.get()
            for event in events:
                if event.type == pg.QUIT:
                    self.quit()

            self._actions_ = []
            for btn in self.btns:
                btn.react_events(events)

            pg.display.flip()

            for action in self._actions_:
                action()
            
            self.clock.tick(self.FPS)

        Music.stop()


class Options:
    FPS = MainMenu.FPS
    def __init__(self, size):
        self.surface = pg.Surface(size)
        w, h = size
        # ajout de l'image

        bg_image = utils.load_image("background_options.jpg")
        bg_image = utils.resize(bg_image, size)
        rect_image = bg_image.get_rect(topleft = (0,0))

        self.surface.blit(bg_image, rect_image)

        font_buttons = pg.font.Font(utils.defaut_font, int(5*CTE))
        self.btn_back = Button((w/4, h/8), (w/2, .7 * h), "BACK", font_buttons, command = self.quit, color = "black", center = True)

        self.build_title(w, h)
        
        self.clock = pg.time.Clock()
        self._running_ = False

    def set_screen(self, screen):
        self.screen = screen

    def build_title(self, w, h):
        text_1 = pg.font.Font(utils.defaut_font, int(3*CTE)).render("This is the OPTIONS screen.", True, "white")
        rect_1 = text_1.get_rect(center = (w/2, .4*h))
        self.surface.blit(text_1, rect_1)

    def quit(self):
        self._running_ = False

    def run(self, screen):
        self._running_ = True
        Music.menu()

        self.set_screen(screen)
        
        while self._running_:
            self.screen.blit(self.surface, (0,0))
            self.btn_back.blit_on(self.screen)
            
            events = pg.event.get()
            for event in events:
                if event.type == pg.QUIT:
                    self.quit()
            self.btn_back.react_events(events)
            
            pg.display.flip()
            self.clock.tick(self.FPS)

if __name__ == "__main__":
    import main
    self = main.app
